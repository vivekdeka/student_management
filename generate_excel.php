<?php
include_once('PHPExcel/Classes/PHPExcel/IOFactory.php');
include_once('connection.php');
//set the desired name of the excel file
$date = new DateTime(date('Y-m-d H:i:s'), new DateTimeZone('Europe/London'));
$fileName = 'reports/student_report_'.strtotime($date->format('Y-m-d H:i:s'));
$sql = "SELECT * FROM tblstudentsdata";
if(isset($_POST['batchCode']))
{
  $sql .= "WHERE batchCode=".$_POST['batchCode'];
}
$result = $conn->query($sql);
if (!empty($result) && $result->num_rows > 0) {
  // Create new PHPExcel object
  $objPHPExcel = new PHPExcel();

  // Set document properties
  $objPHPExcel->getProperties()->setCreator("Me")->setLastModifiedBy("Me")->setTitle("Apollo Students Report")->setSubject("Students Report")->setDescription("Excel Sheet")->setKeywords("Excel Sheet")->setCategory("Me");

  // Set active sheet index to the first sheet, so Excel opens this as the first sheet
  $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1', 'Batch Code')
        ->setCellValue('B1', 'Candidate Name')
        ->setCellValue('C1', 'Father Name')
        ->setCellValue('D1', 'Mother Name')
        ->setCellValue('E1', 'Gender')
        ->setCellValue('F1', 'Address')
        ->setCellValue('G1', 'Block')
        ->setCellValue('H1', 'Dist')
        ->setCellValue('I1', 'State')
        ->setCellValue('J1', 'Mobile Number')
        ->setCellValue('K1', 'Guardian Number')
        ->setCellValue('L1', 'Date Of Birth')
        ->setCellValue('M1', 'Religion')
        ->setCellValue('M1', 'Highest Qualification')
        ->setCellValue('O1', 'Eligibility proof')
        ->setCellValue('P1', 'Card Number')
        ->setCellValue('Q1', 'Bank Name')
        ->setCellValue('R1', 'Branch Name')
        ->setCellValue('S1', 'Account Number')
        ->setCellValue('T1', 'Ifsc Code')
        ->setCellValue('U1', 'Training Start Date')
        ->setCellValue('V1', 'Assessment Date')
        ->setCellValue('W1', 'Training End Date')
        ;

  //Put each record in a new cell
  $i=1;
  //echo "<pre>";
  while($subrow = $result->fetch_assoc())
  {
     $ii = $i+2;
     $objPHPExcel->getActiveSheet()->setCellValue('A'.$ii, $subrow['batchCode']);
     $objPHPExcel->getActiveSheet()->setCellValue('B'.$ii, $subrow['candidateName']);
     $objPHPExcel->getActiveSheet()->setCellValue('C'.$ii, $subrow['fatherName']);
     $objPHPExcel->getActiveSheet()->setCellValue('D'.$ii, $subrow['motherName']);
     $objPHPExcel->getActiveSheet()->setCellValue('E'.$ii, $subrow['gender']);
     $objPHPExcel->getActiveSheet()->setCellValue('F'.$ii, $subrow['address']);
     $objPHPExcel->getActiveSheet()->setCellValue('G'.$ii, $subrow['block']);
     $objPHPExcel->getActiveSheet()->setCellValue('H'.$ii, $subrow['dist']);
     $objPHPExcel->getActiveSheet()->setCellValue('I'.$ii, $subrow['state']);
     $objPHPExcel->getActiveSheet()->setCellValue('J'.$ii, $subrow['mobileNumber']);
     $objPHPExcel->getActiveSheet()->setCellValue('K'.$ii, $subrow['guardianNumber']);
     $objPHPExcel->getActiveSheet()->setCellValue('L'.$ii, $subrow['dateOfBirth']);
     $objPHPExcel->getActiveSheet()->setCellValue('M'.$ii, $subrow['religion']);
     $objPHPExcel->getActiveSheet()->setCellValue('N'.$ii, $subrow['highestQualification']);
     $objPHPExcel->getActiveSheet()->setCellValue('O'.$ii, $subrow['eligibilityProof']);
     $objPHPExcel->getActiveSheet()->setCellValue('P'.$ii, $subrow['cardNumber']);
     $objPHPExcel->getActiveSheet()->setCellValue('Q'.$ii, $subrow['bankName']);
     $objPHPExcel->getActiveSheet()->setCellValue('R'.$ii, $subrow['branchName']);
     $objPHPExcel->getActiveSheet()->setCellValue('S'.$ii, $subrow['accountNumber']);
     $objPHPExcel->getActiveSheet()->setCellValue('T'.$ii, $subrow['ifscCode']);
     $objPHPExcel->getActiveSheet()->setCellValue('U'.$ii, $subrow['trainingStartDate']);
     $objPHPExcel->getActiveSheet()->setCellValue('V'.$ii, $subrow['assessmentDate']);
     $objPHPExcel->getActiveSheet()->setCellValue('W'.$ii, $subrow['trainingEndDate']);
     $i++;
  }
  // Set worksheet title
  $objPHPExcel->getActiveSheet()->setTitle($fileName);

  // Redirect output to a client’s web browser (Excel2007)
  header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
  header('Content-Disposition: attachment;filename="' . $fileName . '.xlsx"');
  header('Cache-Control: max-age=0');

  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
  $objWriter->save('php://output');
  $response['message'] ="Report Generated";
  $response['status'] = "success";
}else{
  $response['message'] ="No students found.";
  $response['status'] = "failed";
}
//echo json_encode($response);
$conn->close();
?>
