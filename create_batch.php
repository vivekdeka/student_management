<?php include_once('includes/header.php');
      include_once('connection.php');
?>
<div class="col-md-12">
  <form>
      <div class="col-md-12 card" >
        <span class="card_heading">Create batch</span>
        <div class="row first_row">
          <div class="col-md-4">
            <div class="form-group">
              <input type="email" class="form-control" id="batchCode" name="batchCode"  placeholder="Enter Batch Code" required>
            </div>
          </div>
        </div>
      </div>
      <div class="row first_row">
        <div class="col-md-4">
          <div class="form-group">
              <button id="createBatch" class="btn btn-primary">Submit</button>
          </div>
        </div>
      </div>
  </form>
</div>
<hr>
<div class="col-md-12">
  <table class="table table-striped table-hover">
    <thead>
      <tr>
        <th>ID</th><th>Batch Code</th><th>Created Date</th><th>Action</th>
      </tr>
    </thead>
    <tbody>
      <?php
          $sql = "SELECT * FROM tblBatchMaster";
          $result = $conn->query($sql);
          if ($result->num_rows > 0) {
              while($row = $result->fetch_assoc()) {
                echo "<tr>";
                echo "<td>".$row['id']."</td>";
                echo "<td>".$row['batchCode']."</td>";
                echo "<td>".$row['createdDate']."</td>";
                echo "<td><a href=''>Delete</a></td>";
                echo "</tr>";
              }
          } else {
              echo "<tr><td colspan=4></td></tr>";
          }
       ?>
    </tbody>
  </table>

</div>
<?php include_once('includes/footer.php');?>
