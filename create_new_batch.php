<?php
  include_once('connection.php');
  if(isset($_POST['batchCode']) && $_POST['batchCode'] != "")
  {
    $batchCode = $_POST['batchCode'];
    $createdDate = date('y-m-d h:m:s');

    $sql = "SELECT * FROM tblBatchMaster";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
      $response['message']="Batch already exist";
      $response['status'] = "failed";
    } else {
      $sql = "INSERT INTO tblBatchMaster (batchCode, createdDate)
            VALUES ('$batchCode', '$createdDate')";

      if ($conn->query($sql) === TRUE) {
        $response['message']="New record created successfully";
        $response['status'] = "success";
      } else {
        $response['message']= $conn->error;
        $response['status'] = "failed";
      }
    }
    echo json_encode($response);
    $conn->close();
  }
 ?>
