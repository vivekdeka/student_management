<?php
  include_once('connection.php');
  $batchCode   = isset($_POST['batchCode'])?$_POST['batchCode']:"";
  $candidateName   = isset($_POST['candidateName'])?$_POST['candidateName']:"";
  $fatherName  = isset($_POST['fatherName'])?$_POST['fatherName']:"";
  $motherName  = isset($_POST['motherName'])?$_POST['motherName']:"";
  $gender  = isset($_POST['gender'])?$_POST['gender']:"";
  $address   = isset($_POST['address'])?$_POST['address']:"";
  $block   = isset($_POST['block'])?$_POST['block']:"";
  $dist  = isset($_POST['dist'])?$_POST['dist']:"";
  $state   = isset($_POST['state'])?$_POST['state']:"";
  $mobileNumber  = isset($_POST['mobileNumber'])?$_POST['mobileNumber']:"";
  $guardianNumber  = isset($_POST['guardianNumber'])?$_POST['guardianNumber']:"";
  $dateOfBirth   = isset($_POST['dateOfBirth'])?$_POST['dateOfBirth']:"";
  $religion  = isset($_POST['religion'])?$_POST['religion']:"";
  $highestQualification  = isset($_POST['highestQualification'])?$_POST['highestQualification']:"";
  $eligibilityProof  = isset($_POST['eligibilityProof'])?$_POST['eligibilityProof']:"";
  $cardNumber  = isset($_POST['cardNumber'])?$_POST['cardNumber']:"";
  $bankName  = isset($_POST['bankName'])?$_POST['bankName']:"";
  $branchName  = isset($_POST['branchName'])?$_POST['branchName']:"";
  $accountNumber   = isset($_POST['accountNumber'])?$_POST['accountNumber']:"";
  $ifscCode  = isset($_POST['ifscCode'])?$_POST['ifscCode']:"";
  $trainingStartDate   = isset($_POST['trainingStartDate'])?$_POST['trainingStartDate']:"";
  $assessmentDate  = isset($_POST['assessmentDate'])?$_POST['assessmentDate']:"";
  $trainingEndDate   = isset($_POST['trainingEndDate'])?$_POST['trainingEndDate']:"";

  $sql = "INSERT INTO tblStudentsData (batchCode, candidateName, fatherName, motherName, gender, address, block, dist, state, mobileNumber,
        guardianNumber, dateOfBirth, religion, highestQualification, eligibilityProof, cardNumber, bankName, branchName,
        accountNumber, ifscCode, trainingStartDate, assessmentDate, trainingEndDate)
        VALUES ('$batchCode', '$candidateName', '$fatherName', '$motherName', '$gender', '$address', '$block', '$dist', '$state', '$mobileNumber',
              '$guardianNumber', '$dateOfBirth', '$religion', '$highestQualification', '$eligibilityProof', '$cardNumber', '$bankName', '$branchName',
              '$accountNumber', '$ifscCode', '$trainingStartDate', '$assessmentDate', '$trainingEndDate')";

  if ($conn->query($sql) === TRUE) {
    $response['message']="New record created successfully";
    $response['status'] = "success";
  } else {
    $response['message']= $conn->error;
    $response['status'] = "failed";
  }
  echo json_encode($response);
  $conn->close();
?>
