$("#submitBtn").on('click', function(e){

  e.preventDefault();
  var batchCode               = $("#batchCode").val().trim();
  if(batchCode == "" || batchCode =="")
  {
     $("#batchCode").focus();
     return false;
  }
  var candidateName           = $("#candidateName").val().trim();
  if(candidateName == "" || candidateName =="")
  {
     $("#candidateName").focus();
     return false;
  }

  var fatherName              = $("#fatherName").val().trim();
  if(fatherName == "" || fatherName =="")
  {
     $("#fatherName").focus();
     return false;
  }
  var motherName              = $("#motherName").val().trim();
  if(motherName == "" || motherName =="")
  {
     $("#motherName").focus();
     return false;
  }
  var gender                  = $('#gender').find(":selected").text();
  if(gender == "" || gender =="")
  {
     $("#gender").focus();
     return false;
  }
  var address                 = $("#address").val().trim();
  var block                   = $("#block").val().trim();
  var dist                    = $("#dist").val().trim();
  var state                   = $("#state").val().trim();
  var mobileNumber            = $("#mobileNumber").val().trim();
  var guardianNumber          = $("#guardianNumber").val().trim();
  var dateOfBirth             = $("#dateOfBirth").val().trim();
  var religion                = $("#religion").val().trim();
  var highestQualification    = $("#highestQualification").val().trim();
  var eligibilityProof        = $('#eligibilityProof').find(":selected").text();
  var cardNumber              = $("#cardNumber").val().trim();
  if(gender == "" || gender =="")
  {
     $("#gender").focus();
     return false;
  }
  var bankName                = $("#bankName").val().trim();
  if(bankName == "")
  {
     $("#bankName").focus();
     return false;
  }

  var branchName              = $("#branchName").val().trim();
  if(branchName == "")
  {
     $("#branchName").focus();
     return false;
  }
  var accountNumber           = $("#accountNumber").val().trim();
  if(accountNumber == "")
  {
     $("#accountNumber").focus();
     return false;
  }

  var ifscCode                = $("#ifscCode").val().trim();
  if(ifscCode == "")
  {
     $("#ifscCode").focus();
     return false;
  }

  var trainingStartDate       = $("#trainingStartDate").val().trim();
  var assessmentDate          = $("#assessmentDate").val().trim();
  var trainingEndDate         = $("#trainingEndDate").val().trim();

  $.ajax({
    url: "process.php",
    type: "POST",
    data: {
      'batchCode':batchCode,
      'candidateName':candidateName,
      'fatherName':fatherName,
      'motherName':motherName,
      'gender':gender,
      'address':address,
      'block':block,
      'dist':dist,
      'state':state,
      'mobileNumber':mobileNumber,
      'guardianNumber':guardianNumber,
      'dateOfBirth':dateOfBirth,
      'religion':religion,
      'highestQualification':highestQualification,
      'eligibilityProof':eligibilityProof,
      'cardNumber':cardNumber,
      'bankName':bankName,
      'branchName':branchName,
      'accountNumber':accountNumber,
      'ifscCode':ifscCode,
      'trainingStartDate':trainingStartDate,
      'assessmentDate':assessmentDate,
      'trainingEndDate':trainingEndDate
    },
    dataType: "json", 	//Expected data format from server
    success: function (res) {//On Successful service call
        $(window).scrollTop($(".success_message").offset().top);
        if(res.status == "success")
        {
          $(".success_message").css('display','block');
          $(".success_message").html(res.message);
          $(".success_message").focus();
        }else{
          $(".error_message").css('display','block');
          $(".error_message").html(res.message);
          $(".error_message").focus();
        }
        location.reload();
    }
  });
});

$("#createBatch").on('click', function(e){
  e.preventDefault();
  var batchCode               = $("#batchCode").val().trim();
  if(batchCode == "" || batchCode =="")
  {
     $("#batchCode").focus();
     return false;
  }
  $.ajax({
    url: "create_new_batch.php",
    type: "POST",
    data: {
      'batchCode':batchCode
    },
    dataType: "json", 	//Expected data format from server
    success: function (res) {//On Successful service call
      if(res.status == "success")
        {
          $(".success_message").css('display','block');
          $(".success_message").html(res.message);
          $(".success_message").focus();
        }else{
          $(".error_message").css('display','block');
          $(".error_message").html(res.message);
          $(".error_message").focus();
        }
        location.reload();
    }
  });
});

$('#reportByForm input').on('change', function() {
   $reportType = $('input[name=optradio]:checked', '#reportByForm').val();

   if($reportType == "byBatch")
   {
     $("#view_report_batchCode").prop("disabled", false);
     $("#view_report_Btn").prop("disabled", true);
   }else if($reportType == "allStudents"){
     $("#view_report_Btn").prop("disabled", false);
     $("#view_report_batchCode").prop("disabled", true);
   }
});

$("#view_report_Btn").on('click', function(e){
  e.preventDefault();
  $.ajax({
    url: "generate_excel.php",
    type: "POST",
    dataType: "json", 	//Expected data format from server
    success: function (res) {//On Successful service call
      alert(res.message);
    }
  });
});

$("#view_report_batchCode").on('change', function(e){
  var batchcode = $( "#view_report_batchCode option:selected" ).text();
  e.preventDefault();
  $.ajax({
    url: "generate_excel.php",
    type: "POST",
    dataType: "json", 	//Expected data format from server
    data: {
      'batchCode':batchcode
    },
    success: function (res) {//On Successful service call
      alert(res.message);
    }
  });
});
