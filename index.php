
<?php
      include_once('includes/header.php');
      include_once('connection.php');
?>

<div class="col-md-12">
    <form>
        <div class="col-md-12 card" >
          <span class="card_heading">Students and Parents Details</span>
          <div class="row first_row">
            <div class="col-md-4">
              <div class="form-group">
                <?php
                    $sql = "SELECT * FROM tblBatchMaster";
                    $result = $conn->query($sql);

                    echo"<select class='form-control' id='batchCode' name='batchCode' required>";
                    echo "<option value='' selected>Select Batch Code</option>";
                    if ($result->num_rows > 0) {
                        while($row = $result->fetch_assoc()) {
                           echo"<option value=".$row['batchCode'].">".$row['batchCode']."</option>";
                        }
                    } else {
                        echo "<option value=''>No batch found</option>";
                    }
                    echo "</select>";
                ?>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <input type="text" class="form-control" id="candidateName" name="candidateName"  placeholder="Enter Candidate Name " required>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <input type="text" class="form-control" id="fatherName" name="fatherName"  placeholder="Enter Father's  Name" required>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <input type="text" class="form-control" id="motherName" name="motherName"  placeholder="Enter Mother's Name" required>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <select class="form-control" id="gender" name="gender" required>
                 <option value='' selected>Select Gender</option>
                 <option value='male'>Male</option>
                 <option value='female'>Female</option>
               </select>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <input type="text" class="form-control" id="dateOfBirth" name="dateOfBirth"  placeholder="Enter Date of Birth. Eg: yyyy/mm/dd">
              </div>
            </div>
          </div>
        </div>

        <!-- Address Details -->
        <div class="col-md-12 card">
          <span class="card_heading">Address Details</span>
          <div class="row first_row">
            <div class="col-md-4">
              <div class="form-group">
                <input type="text" class="form-control" id="address" name="address"  placeholder="Enter Address">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <input type="text" class="form-control" id="block" name="block"  placeholder="Enter Block">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <input type="text" class="form-control" id="dist" name="dist"  placeholder="Enter Dist">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <input type="text" class="form-control" id="state" name="state"  placeholder="Enter State">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <input type="text" class="form-control" id="mobileNumber" name="mobileNumber"  placeholder="Enter Mobile Number">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <input type="text" class="form-control" id="guardianNumber" name="guardianNumber"  placeholder="Enter Guardian No">
              </div>
            </div>
          </div>
        </div>

        <div class="col-md-12 card">
          <span class="card_heading">Edication Details</span>
          <div class="row first_row">
            <div class="col-md-4">
              <div class="form-group">
                <input type="text" class="form-control" id="religion" name="religion"  placeholder="Enter Religion">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <input type="text" class="form-control" id="highestQualification" name="highestQualification"  placeholder="Enter Highest Qualification">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <select class="form-control" id="eligibilityProof" name="eligibilityProof">
                 <option value='' selected>Select Eligibility proof</option>
                 <option value='bpl'>BPL (Below Poverty Line)</option>
                 <option value='jc'>Job Card</option>
                 <option value='shg'>SHG (Self Help Group)</option>
               </select>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <input type="text" class="form-control" id="cardNumber"  placeholder="Enter Eligibility proof Card No">
              </div>
            </div>
          </div>
        </div>

        <!-- Bank Details -->
        <div class="col-md-12 card">
          <span class="card_heading">Bank Details</span>
          <div class="row first_row">

            <div class="col-md-4">
              <div class="form-group">
                <input type="text" class="form-control" id="bankName"  placeholder="Enter Bank Name">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <input type="text" class="form-control" id="branchName"  placeholder="Enter Branch name">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <input type="text" class="form-control" id="accountNumber"  placeholder="Enter Account Number">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <input type="text" class="form-control" id="ifscCode"  placeholder="Enter IFSC Code">
              </div>
            </div>
          </div>
        </div>
        <!-- Training Details -->
        <div class="col-md-12 card">
          <span class="card_heading">Training Details</span>
          <div class="row first_row">
            <div class="col-md-4">
              <div class="form-group">
                <input type="text" class="form-control" id="trainingStartDate" name="trainingStartDate"  placeholder="Enter Training Start Date">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <input type="text" class="form-control" id="assessmentDate" name="assessmentDate"  placeholder="Enter Assessment Date">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <input type="text" class="form-control" id="trainingEndDate" name="trainingEndDate"  placeholder="Enter Training End Date">
              </div>
            </div>
          </div>
        </div>
        <div class="row first_row">
              <div class="col-md-4">
                <div class="form-group">
                    <button id="submitBtn" class="btn btn-primary">Submit</button>
                </div>
              </div>
            </div>
    </form>
</div>
<?php include_once('includes/footer.php');?>
