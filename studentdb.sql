-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 22, 2018 at 04:06 PM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `studentdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `tblbatchmaster`
--

CREATE TABLE `tblbatchmaster` (
  `id` int(11) NOT NULL,
  `batchCode` varchar(255) DEFAULT NULL,
  `createdDate` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblbatchmaster`
--

INSERT INTO `tblbatchmaster` (`id`, `batchCode`, `createdDate`) VALUES
(1, 'BS15487', '2018-02-17');

-- --------------------------------------------------------

--
-- Table structure for table `tblstudentsdata`
--

CREATE TABLE `tblstudentsdata` (
  `id` int(11) NOT NULL,
  `batchCode` varchar(50) NOT NULL,
  `candidateName` varchar(255) NOT NULL,
  `fatherName` varchar(255) NOT NULL,
  `motherName` varchar(255) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `block` varchar(50) DEFAULT NULL,
  `dist` varchar(100) DEFAULT NULL,
  `state` varchar(150) DEFAULT NULL,
  `mobileNumber` varchar(11) DEFAULT NULL,
  `guardianNumber` varchar(11) DEFAULT NULL,
  `dateOfBirth` date DEFAULT NULL,
  `religion` varchar(50) DEFAULT NULL,
  `highestQualification` varchar(255) DEFAULT NULL,
  `eligibilityProof` varchar(255) DEFAULT NULL,
  `cardNumber` varchar(60) DEFAULT NULL,
  `bankName` varchar(50) DEFAULT NULL,
  `branchName` varchar(100) DEFAULT NULL,
  `accountNumber` varchar(50) DEFAULT NULL,
  `ifscCode` varchar(50) DEFAULT NULL,
  `trainingStartDate` date DEFAULT NULL,
  `assessmentDate` date DEFAULT NULL,
  `trainingEndDate` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblstudentsdata`
--

INSERT INTO `tblstudentsdata` (`id`, `batchCode`, `candidateName`, `fatherName`, `motherName`, `gender`, `address`, `block`, `dist`, `state`, `mobileNumber`, `guardianNumber`, `dateOfBirth`, `religion`, `highestQualification`, `eligibilityProof`, `cardNumber`, `bankName`, `branchName`, `accountNumber`, `ifscCode`, `trainingStartDate`, `assessmentDate`, `trainingEndDate`) VALUES
(1, 'B1458569', 'Vivek', 'Binoy', 'Manomti', 'Male', 'House 478', 'B Block', 'Kolkata', 'WB', '8420269852', '7845896521', '1989-10-15', 'Hindu', 'BSC', 'Job Card', '7845896587458745', 'HDFC', 'Kolkata', '78451258965887412545', 'FVCS4578954', '2018-02-27', '0000-00-00', '2018-03-05'),
(2, 'B1458569', 'Vivek', 'Binoy', 'Manomti', 'Male', 'House 478', 'B Block', 'Kolkata', 'WB', '8420269852', '7845896521', '1989-10-15', 'Hindu', 'BSC', 'Job Card', '7845896587458745', 'HDFC', 'Kolkata', '78451258965887412545', 'FVCS4578954', '2018-02-27', '2018-02-28', '2018-03-05'),
(3, 'B1458569', 'Vivek', 'Binoy', 'Manomti', 'Male', 'House 478', 'B Block', 'Kolkata', 'WB', '8420269852', '7845896521', '1989-10-15', 'Hindu', 'BSC', 'Job Card', '7845896587458745', 'HDFC', 'Kolkata', '78451258965887412545', 'FVCS4578954', '2018-02-27', '2018-02-28', '2018-03-05'),
(4, 'asdasd', 'asdasd', 'asdasd', 'asdasd', 'Male', 'ASas', 'asas', 'zzxzx', 'ZXZX', 'ZXZX', 'ZXZX', '1989-12-15', 'ZXZX', 'ZXZX', 'Job Card', 'ZXZX', 'ZXZX', 'ZXZX', 'ZXZX', 'ZXZ', '0000-00-00', '0000-00-00', '0000-00-00'),
(5, 'asdasd', 'asdasd', 'asdasd', 'asdasd', 'Female', 'asdsadasd', 'asdasd', 'asdasd', 'asdasd', 'asdasd', 'asdasd', '1975-12-15', 'asdasd', 'asdasd', 'Job Card', 'asdasd', 'asdasd', 'asdasd', 'asdasd', 'asdasd', '2018-12-14', '2018-12-14', '2018-12-14'),
(6, 'zdsdf', 'fsdfsdf', 'sdfsdf', 'sdfsdf', 'Female', 'asdasd', 'asdasd', 'asdasd', 'asdasd', 'asdasd', 'asdasd', '0000-00-00', 'asdasd', 'asdasd', 'Job Card', 'asdasd', 'asdasd', 'asdasd', 'asdasd', 'asdasd', '0000-00-00', '0000-00-00', '0000-00-00'),
(7, 'Asads', 'dsadasd', 'asdasd', 'asdasd', 'Female', 'adasd', 'asdasd', 'asdasd', 'asdasd', 'asdasd', 'asdasd', '2018-12-15', 'asdasd', 'asdasd', 'Job Card', 'asdasd', 'asdasd', 'asdasd', 'asdasd', 'asdas', '2018-12-15', '2018-12-15', '2018-12-15'),
(8, 'SDasd', 'asdasd', 'asdasd', 'asdasd', 'Male', 'sdasdasd', 'asdasd', 'asdasd', 'asdasd', 'asdasd', 'asdasd', '2018-12-15', 'asdasd', 'asdas', 'SHG (Self Help Group)', 'asdasd', 'asdasd', 'asdasd', 'asdasd', 'asdasd', '2018-12-15', '2018-12-15', '2018-12-15'),
(9, 'SDasd', 'asdasd', 'asdasd', 'asdasd', 'Male', 'sdasdasd', 'asdasd', 'asdasd', 'asdasd', 'asdasd', 'asdasd', '2018-12-15', 'asdasd', 'asdas', 'SHG (Self Help Group)', 'asdasd', 'asdasd', 'asdasd', 'asdasd', 'asdasd', '2018-12-15', '2018-12-15', '2018-12-15'),
(10, 'asdasd', 'asdasd', 'asdasd', 'asdasd', 'Female', 'asdasd', 'asdasd', 'asdasd', 'asdasd', 'asdasd', 'asdad', '2018-12-15', 'asdas', 'asdasd', 'BPL (Below Poverty Line)', 'asdasd', 'asdasd', 'asdasd', 'asdasd', 'asdasd', '2018-12-15', '2018-12-15', '2018-12-15'),
(11, 'asdasd', 'asdasd', 'asdas', 'sdad', 'Female', 'asdasda', 'sdasdas', 'dasdasd', 'asdasd', 'asdasd', 'asdasd', '2018-12-15', 'asdasd', 'asdasd', 'Job Card', 'asdasd', 'sadasd', 'asdasd', 'asdasd', 'asdasd', '2018-12-15', '2018-12-15', '2018-12-15'),
(12, 'adasd', 'asdasd', 'asdasd', 'asdas', 'Male', 'asdasd', 'asdasd', 'asdasd', 'asdasd', 'asdasd', 'asdasd', '2018-08-12', 'asdasd', 'asdasd', 'BPL (Below Poverty Line)', 'asdasd', 'asdasd', 'asdasd', 'asdasd', 'asdasd', '2018-08-12', '2018-08-12', '2018-08-12');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tblbatchmaster`
--
ALTER TABLE `tblbatchmaster`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblstudentsdata`
--
ALTER TABLE `tblstudentsdata`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tblbatchmaster`
--
ALTER TABLE `tblbatchmaster`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tblstudentsdata`
--
ALTER TABLE `tblstudentsdata`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
