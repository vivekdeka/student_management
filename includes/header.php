<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Students Data</title>
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <style media="screen">
      .card{
            margin-bottom: 3%;
      }
      .first_row{
            margin-top: 1.5%;
      }
      .card_heading{
        margin-bottom: -15px;
        color:#9c9e9e;
      }
      .success_message{
        background: green;
        font-weight: bold;
        color:#fff;
        display: none;
      }

      .error_message{
        background: red;
        color:#fff;
        font-weight: bold;
        display: none;
      }
    </style>
  </head>
  <body>
    <div class="container">
      <div class="row">
        <div class="col-md-12" style="text-align:center; margin-top: 2%; margin-bottom: 2%;">
          <h1>Apollo Students Form</h1>
          <hr/>
          <a href="create_batch.php">Manage Batch</a> | <a href="index.php">Add Student to Batch</a> | <a href="view_report.php">View Reports</a>
          <br>
          <span class="success_message" style="width: 38%;"></span>
          <span class="error_message"   style=" width: auto;"></span>
        </div>
      </div>
