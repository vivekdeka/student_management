<?php
  include_once('includes/header.php');
  include_once('connection.php');
?>
<div class="col-md-12">
  <span class="card_heading">You can download students report by batch or all students at a time</span>
  <div class="row">
    <div class="col-md-12">
      <form id="reportByForm">
        <div class="radio">
          <label><input type="radio" name="optradio" value="byBatch">By Batch</label>
        </div>
        <div class="radio">
          <label><input type="radio" name="optradio" value="allStudents">All Students</label>
        </div>
      </form>
    </div>
  </div>
  <div class="row first_row">
    <div class="col-md-4">
      <div class="form-group">
        <?php
            $sql = "SELECT * FROM tblBatchMaster";
            $result = $conn->query($sql);
            echo"<select class='form-control' id='view_report_batchCode' name='batchCode' required disabled>";
            echo "<option value='' selected>Select Batch Code</option>";
            if ($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) {
                   echo"<option value=".$row['batchCode'].">".$row['batchCode']."</option>";
                }
            } else {
                echo "<option value=''>No batch found</option>";
            }
            echo "</select>";
        ?>
      </div>
    </div>
    <div class="col-md-4">
      <button class="btn btn-default" disabled id='view_report_Btn'>All Students</button>
      <span id="download_link"></span>
    </div>
  </div>
</div>
<?php include_once('includes/footer.php');?>
