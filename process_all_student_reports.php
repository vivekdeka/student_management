<?php
  include_once('connection.php');
  include_once('PHPExcel/Classes/PHPExcel/IOFactory.php');
  include_once('PHPExcel/Classes/PHPExcel.php');
  $sql = "SELECT * FROM tblstudentsdata";
  $result = $conn->query($sql);
  if ($result->num_rows > 0) {
    //define separator (defines columns in excel & tabs in word)
    $sep = "\t"; //tabbed character
    $fileName = "reports/".strtotime("now").".xlsx";
    $objPHPExcel = new PHPExcel();
    $row = 1;
    while($subrow = $result->fetch_assoc())
    {
         $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $subrow['batchCode']);
         $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $subrow['candidateName']);
         $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $subrow['fatherName']);
         $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $subrow['motherName']);
         $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $subrow['gender']);
         $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $subrow['address']);
         $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $subrow['block']);
         $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $subrow['dist']);
         $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $subrow['state']);
         $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $subrow['mobileNumber']);
         $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $subrow['guardianNumber']);
         $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $subrow['dateOfBirth']);
         $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $subrow['religion']);
         $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $subrow['highestQualification']);
         $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $subrow['eligibilityProof']);
         $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $subrow['cardNumber']);
         $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $subrow['bankName']);
         $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $subrow['branchName']);
         $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $subrow['accountNumber']);
         $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $subrow['ifscCode']);
         $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $subrow['trainingStartDate']);
         $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $subrow['assessmentDate']);
         $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $subrow['trainingEndDate']);
         $row++;
    }
    $excelWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $excelWriter->save($fileName);
  } else {
    $response['fileName'] ="";
    $response['status'] = "failed";
  }
  echo json_encode($response);
  $conn->close();
?>
